console.log('Cargando Cards...');
const dataCards = [

    {
        "title": "Casa 2 cuartos",
        "url_image": "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/apartamento-luminoso-decorado-con-macrame6-1591781964.jpg?resize=480:*",
        "desc": "Un cuarto, baño y pila e incluyendo los servicios de agua, basura y cable. /n/nPrecio: 80000 colones.",
        
        
     },
     {
        "title": "Casa 3 cuartos",
        "url_image": "https://ciudadpyme.com/wp-content/uploads/2020/08/JAP_6030-thegem-product-single.jpg",
         "desc": "Un cuarto, desayunador, sala, baño y cuarto pilas e incluyendo los servicios de agua, basura y cable. /n/nPrecio en promoción: 110000 colones. /n/nPrecio normal: 130000 colones.",
       
        
     }];

     (function(){
        let CARD = {
            init: function(){
                console.log('El modulo de carga funciona correctamente');
                let _self=this;

                //llamamos a las funciones
                this.insertData(_self);
            
               },

            eventHandler: function (_self){
                let arrayRefs = document.querySelectorAll('.accordion-title');

                for(let x =0; x < arrayRefs.length; x++){
                    arrayRefs[x].addEventListener('click', function(event){
                        console.log('Evento: ', event)
                        _self.showTab(event.target);
                    
                       });
                
                   }
            
               },

                   insertData: function (_self){
                       dataCards.map(function (item, index){
                           document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
                       
                       });

                   
                   },

                       tplCardItem: function(item, index){
                           return(`<div class='card-item' id="card-number-${index}">
                           <img src= "${item.url_image}"/>
                           <div class="card-info">
                               <p class= 'card-title'> ${item.title}</p>
                               <p class= 'card-desc'> ${item.desc}</p>

                               </div>
                           </div>`)
                       
                       },
   
                   }

   CARD.init();

})();