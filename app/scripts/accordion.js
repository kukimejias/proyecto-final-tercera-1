console.log('Cargando Accordion...');
const dataAccordion = [
    {
        "title": "DONDE ESTAMOS UBICADOS",
        "desc": "Nuestro horario de atención es de 9a.m a 5p.m. /n/nUbicados en La Guaria, Canoas Alajuela, contiguo a la iglesia Santa Eduviges."
    },
    {
        "title": "CONTACTANOS",
        "desc": "Cualquier consulta o duda, contáctanos al: 24406635"
    },
];

(function () {
    let ACCORDION = {
        init: function () {
            let _self = this;
            //llamamos las funciones
            this.insertData(_self);
            this.eventHandler(_self);
        },

        eventHandler: function (_self) {
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++) {
                arrayRefs[x].addEventListener('click', function (event) {
                    console.log('event', event);
                    _self.showTab(event.target);
                });
            }
        },

        showTab: function (refItem) {
            let activeTab = document.querySelector('.tab-active');

            if (activeTab) {
                activeTab.classList.remove('tab-active');
            }

            console.log('show tab', refItem);
            refItem.parentElement.classList.toggle('tab-active');
        },

        insertData: function (_self) {
            dataAccordion.map(function (item, index) {
                document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend', _self.tplAccordionItem(item));
            });
        },

        tplAccordionItem: function (item) {
            return (`<div class='accordion-item'>
            <div class='accordion-title'><p>${item.title}</p></div>
            <div class='accordion-desc'><p>${item.desc}</p></div>
            </div>`)
        },


    }
    ACCORDION.init();
})();
